# Install
```bash
pnpm i
```

# Um index.ts auszuführen: 
```bash
pnpm start-ts
```

# Um index.js auszuführen:
```bash
pnpm start-js
```
