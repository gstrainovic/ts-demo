'use strict';

// https://github.com/denysdovhan/wtfjs

// Beispiele von JavaScript Macken

// Die meisten Programmiersprachen würden einen Fehler auslösen, wenn diese Art von Fehlern auftreten, 
// einige würden dies während der Kompilierung tun – bevor Code ausgeführt wird.
// Beim Schreiben kleiner Programme sind solche Macken ärgerlich, aber beherrschbar,
// aber in großen Programmen können sie zu schwer zu findenden Fehlern führen.

// JavaScript ermöglicht den Zugriff auf Eigenschaften, die nicht vorhanden sind:
const obj = { x: 10, y: 15 };
const area = obj.x * obj.z;
console.log(area); // Es kommt kein Fehler das z nicht vorhanden ist, sondern es kommt NaN raus.

// JavaScript benutzt den + Operator für die Addition und für die Verkettung von Strings.

// addiert zwei Zahlen
const result = 1 + 2;
console.log(result); // 3

// verbindet zwei Strings
const string = 'a' + 'b';
console.log(string); // ab

// addiert eine Zahl und einen String
const result2 = 1 + '2';
console.log(result2); // 12

// Arrays addieren
const result3 = [1, 2] + [3, 4];
console.log(result3); // 1,23,4

// Vergleichen von drei Nummern
const result4 = 1 < 2 < 3;
console.log(result4); // true

const result5 = 3 > 2 > 1;
console.log(result5); // false

// Funny math
let a = 3 - 1  // -> 2
let b = 3 + 1  // -> 4
let c = '3' - 1  // -> 2
let d = '3' + 1  // -> '31' // Wobei man sagen muss das TS dies auch macht, wenn man den Typ nicht angibt.
let e = true + true  // -> 2

// Sortieren von einem Array
const arr = [10, 1, 3];
console.log(arr.sort()); // [1, 10, 3]




