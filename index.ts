// TypeScript ist eine Erweiterung von JavaScript.
// TypeScript ist JavaScript mit Typen.
// TypeScript wird zu JavaScript kompiliert.
// Bei Neuentwicklungen welche auf dem Server laufen, wird TypeScript gegenüber JavaScript bevorzugt.
// Browser können leider kein TypeScript ausführen.

// TypeScript erkennt wenn möglich den Datentyp automatisch.
// Hier wird der Datentyp automatisch auf string gesetzt.
// eslint-disable-next-line prefer-const
let text = 'Hello World'
console.log(text)

// eslint-disable-next-line prefer-const
let myNumber = 1
console.log(myNumber)

// eslint-disable-next-line prefer-const
let myBoolean = true
console.log(myBoolean)

// eslint-disable-next-line prefer-const
let myArray = [1, 2, 3]
console.log(myArray)

// eslint-disable-next-line prefer-const
let myObject = { name: 'Max' }
console.log(myObject)

// Falls der Datentyp nicht automatisch erkannt wird,
// dann kann man den Datentyp auch explizit angeben mit : Datentyp
const text2: string = 'Hello World'
console.log(text2)

const myNumber2: number = 1
console.log(myNumber2)

const myBoolean2: boolean = true
console.log(myBoolean2)

const myArray2: number[] = [1, 2, 3]
console.log(myArray2)

const myObject2: { name: string } = { name: 'Max' }
console.log(myObject2)

// Warum sind Datentypen wichtig?
// TypeScript kann Fehler erkennen und uns darauf hinweisen.
// Beispiel: Wir haben eine Funktion, die zwei Zahlen addiert.
// Wenn wir eine Zahl und einen String übergeben, dann gibt es einen Fehler.
function add(a: number, b: number): number {
  return a + b
}

add(1, 2)
// add(1, '2') // TODO: Wieder auskommentieren

// TypeScript kann uns helfen bei Objekten.
// Beispiel: Wir haben ein Objekt mit dem Namen user.
// Wir wollen den Namen des Users ausgeben.
const user = {
  name: 'Max',
  age: 30
}

console.log(user.name)
// console.log(user.alter // TODO: Wieder auskommentieren

// Interface
// Wir definieren ein Interface mit dem Namen User.
// Das Interface hat zwei Properties: name und age.
// Age ist optional, weil wir ein ? hinter age schreiben.
interface User {
  name: string
  age?: number
}

// Wir erstellen ein Objekt vom Typ User.
const user2: User = {
  name: 'Max',
  age: 30
}
console.log(user2.name)

// Wenn wir das Alter nicht angeben, dann gibt es keinen Fehler.
// TypeScript erkennt, dass das Alter optional ist.
const user3: User = {
  name: 'Max'
}
console.log(user3.name)

// Wenn wir ein Property angeben, das nicht im Interface definiert ist,
// dann gibt es einen Fehler.
const user4: User = {
  name: 'Max'
  // alter: 30 // TODO: Wieder auskommentieren
}
console.log(user4.name)

// Interface kann man verwenden, um Parameter und Rückgabewerte von Funktionen zu definieren.
// Beispiel mit der Rückagbe von einer Funktion mit dem Typ User.
function getUser(): User {
  return {
    name: 'Max',
    age: 30
  }
}
console.log(getUser())

// Wir können auch bei einem Array den Datentyp definieren.
// Beispiel: Ein Array aus Zahlen.
const numbers: number[] = [1, 2, 3]
console.log(numbers)
// const numbers2: number[] = [1, '2', 3] // TODO: Wieder auskommentieren

// Beispiel: Ein Array aus Zahlen oder Strings.
const numbersOrStrings: Array<number | string> = [1, '2', 3]
console.log(numbersOrStrings)

// JavaScript Fails bei TypeScript
// Arrays addieren
// const result3 = [1, 2] + [3, 4]; // TS erkennt den Fehler den JS nicht erkennt // TODO: Wieder auskommentieren
// console.log(result3) // 1,23,4

// Vergleichen von drei Nummern
// const result4 = 1 < 2 < 3 // TS erkennt den Fehler den JS nicht erkennt // TODO: Wieder auskommentieren
// console.log(result4) // true

// const result5 = 3 > 2 > 1; // TS erkennt den Fehler den JS nicht erkennt // TODO: Wieder auskommentieren
// console.log(result5) // false

// Funny math
// const a = 3 - 1 // -> 2
// const b = 3 + 1 // -> 4
// const c = '3' - 1 // -> 2 // TS erkennt den Fehler den JS nicht erkennt // TODO: Wieder auskommentieren
// const d = '3' + 1 // -> '31' // TS erkennt den Fehler nicht, da es d als string interpretiert
// const dd: number = '3' + 1 // TS erkennt den Fehler, da wir dd als number definieren // TODO: Wieder auskommentieren
// const e = true + true  // -> 2 // TS erkennt den Fehler den JS nicht erkennt // TODO: Wieder auskommentieren
// console.log(a, b, c, d, dd, e)

// Sortieren von einem Array, TS erkennt den Fehler den JS nicht erkennt
// const arr = [10, 1, 3]
// console.log(arr.sort()) // [1, 10, 3] // TODO: Wieder auskommentieren
